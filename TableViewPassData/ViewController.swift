////  ViewController.swift
//  TableViewPassData
//
//  Created on 03/09/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

struct Category {
    let title: String
    let item: [String]
}

class ViewController: UIViewController {
    
    private let data: [Category] = [
        Category(title: "Fruits", item: ["Apple", "Banana", "Orange"]),
        Category(title: "Cars", item: ["BMW","Audi","VW"]),
        Category(title: "Cities", item: ["Warszawa","Krakow","Opole"])
    ]
    
    private let tableView: UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.addSubview(tableView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let category = data[indexPath.row]
        
        // Pass data to ListView controller
        let vc = ListViewController(items: category.item)
        vc.title = category.title
        navigationController?.pushViewController(vc, animated: true)
    }
}
